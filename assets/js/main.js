var menu = {
    close: function(){        
        setTimeout(function(){
            $(".menu-btn").removeClass("active");
            $("nav.main-menu").removeClass("active");
            $("nav.main-menu ul").css("left","");
            $("nav.main-menu ul").removeClass("active");
        },200); 
    },
    open: function(){
        //si no agregar las clases activo a menu y boton
        $("nav.main-menu ul").removeClass("men");
        $(".menu-btn").addClass("active");
        $("nav.main-menu").addClass("active");
        setTimeout(function(){
            $("nav.main-menu ul").addClass("active");
        },200);
    }
}
$(document).ready(function(e) {
        $("nav.main-menu ul").draggable({
            axis:"x",
            stop: function(e,ui){
                var left = ui.position.left;
                if(left < -30){
                    $("nav.main-menu ul").removeClass("may");
                    $("nav.main-menu ul").addClass("men");
                    setTimeout(function(){
                        menu.close();
                    },200);
                }else{
                    $("nav.main-menu ul").addClass("may"); 
                }
            },
            drag: function(e,ui){
                var left = ui.position.left;
                if(left > 0){
                    $("nav.main-menu ul").addClass("may");                    
                }else{
                    $("nav.main-menu ul").removeClass("may");
                }
            }            
        });
	$(".menu-btn").on("click",function(e){
            e.preventDefault();
            //if es activo quitar clases activo a boton y menu
            if($(".menu-btn").hasClass("active")){
                menu.close();             
            }else{
                //si no agregar las clases activo a menu y boton
                menu.open();               
            }
            
        });        
        $(document).on("scroll",function(e){
            var windowTop = $(document).scrollTop();
            if(windowTop > 100){                
                $(".page>.box-1>header").addClass("fix");
                $(".banner-content .btn").appendTo("header");
            }else{
                $(".page>.box-1>header").removeClass("fix");
                $("header>.btn").appendTo(".banner-content");
            }
            
            windowTop = $(document).scrollTop();
            var windowBottom = windowTop + window.innerHeight;
            
            var items = $(".to-animate");
            for(var i = 0; i<items.length;i++){
                var item = items[i];
                if(windowBottom >= $(item).offset().top){
                    $(item).addClass("animate");
                }else{
                    $(item).removeClass("animate");
                }
            }
            
            
        });
});